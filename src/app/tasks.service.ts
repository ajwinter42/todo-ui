import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Task} from './task'

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  constructor(
    private readonly http: HttpClient,
  ) {
  }

  public getAll(): Observable<Task[]> {
    return this.http.get<Task[]>('http://localhost:8000/api/tasks')
  }
}
