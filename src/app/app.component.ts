import {Component, OnInit} from '@angular/core';
import {Task} from './task'
import {TasksService} from './tasks.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  tasks!: Task[]

  constructor(
    private readonly tasksService: TasksService
  ) {
  }

  ngOnInit(): void {
    this.tasksService.getAll().subscribe(data => {
      this.tasks = data
    })
  }
}
